# mha-example-for-aws

[MHA for MySQL](https://code.google.com/p/mysql-master-ha/)をAWSで動かすExample Projectです。

各サーバでスクリプトを実行しミドルウェア等のセットアップを行います。

## Diagram

![Diagram](https://bitbucket.org/winebarrel/mha-example-for-aws/raw/master/diagram.png)

## Settings

* RouteTable VIPによるフェイルオーバー
* Upstartによるデーモン化
* オンラインフェイルオーバーの実装
* Example Rails アプリケーション

## Preparation

* Amazon Linuxの使用を前提としています
* amzn-ami-pv-2013.09.2.x86_64-ebs (`ami-0d13700c`)
* UserDataに以下を入力してリポジトリのバージョンを固定してください ([参考](http://dev.classmethod.jp/cloud/aws/amazon-linux-ami-2014-03-trap/))

```
#cloud-config
repo_releasever: 2013.09
```

* 相互に通信可能なインスタンスを５台作成してください
* ぞれぞれのインスタンスのNameタグを以下に設定してください

`cthulhu` MySQL Master

`hastur` `yog` MySQL Slave

`yog` MHA Manager

`dagon` Example Rails アプリケーション

* 各サーバにログインして、上から順番に **Installation** を実行します

## Installation

### cthulhu(最初), hastur, nyar, yog, dagon(最後)

```sh
export AWS_ACCESS_KEY_ID=...
export AWS_SECRET_ACCESS_KEY=...

sudo yum install -y git
git clone https://bitbucket.org/winebarrel/mha-example-for-aws.git
NAME=`./mha-example-for-aws/bin/describe-tags | awk '/Name/{print $2}'`

cd mha-example-for-aws/servers/$NAME
./setup.sh # dagonでは完了まで時間がかかります

if [ "$NAME" = "yog" ]; then
  sudo masterha_check_ssh --conf=/etc/app1.cnf
  sudo masterha_check_repl --conf /etc/app1.cnf
  ./setup-virtual-ip-address.sh
fi
```

※`cthulhu` `hastur` `yog` のloに仮想IPが追加されます。再起動後は手動で追加してください

## Starting MHA Manager

### yog

```sh
sudo masterha_manager --conf=/etc/app1.cnf
# 以下のコマンドでmanagerを停止します
# sudo masterha_stop --conf=/etc/app1.cnf
```

### (as daemon)

```sh
sudo initctl start mha
```

## Master IP online change

### yog

```sh
# managerが起動している場合は事前に停止してください
sudo masterha_master_switch --master_state=alive --conf=/etc/app1.cnf
```

## Test of Master IP online change

### dagon

```sh
cd ~/hello
bundle exec unicorn_rails -E production -c config/unicorn.rb -D
siege localhost/items

# 以下のコマンドでunicornを停止します
# kill `cat /tmp/unicorn.pid`
```

### yog

```sh
sudo masterha_master_switch --master_state=alive --conf=/etc/app1.cnf

# 以下のコマンドでレプリケーション構成を元に戻します
# restore-repl
```

## Check MHA

### yog

```sh
sudo masterha_check_ssh --conf=/etc/app1.cnf
sudo masterha_check_repl --conf /etc/app1.cnf
```

## MHA Demo

* [http://asciinema.org/a/6939](http://asciinema.org/a/6939)

## Tools

* [`/usr/local/bin/create-route`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/create-route) RouteTableへ仮想IP（ルート）を登録
* [`/usr/local/bin/delete-route`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/delete-route) RouteTableから仮想IP（ルート）を削除
* [`/usr/local/bin/describe-instances`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/describe-instances) インスタンス一覧を表示
* [`/usr/local/bin/describe-network-interfaces`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/describe-network-interfaces) ENI一覧を表示
* [`/usr/local/bin/describe-route-tables`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/describe-route-tables) RouteTableのエントリ一覧を表示
* [`/usr/local/bin/describe-source-dest-check`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/describe-source-dest-check) Source/Desc Checkの状態を表示
* [`/usr/local/bin/describe-tags`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/describe-tags) タグを表示
* [`/usr/local/bin/modify-source-dest-check`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/modify-source-dest-check) Source/Desc Checkの状態を変更
* [`/usr/local/bin/replace-route`](https://bitbucket.org/winebarrel/mha-example-for-aws/src/master/bin/replace-route) RouteTableの仮想IP（ルート）を書き換え

## Link
* [mysql-master-ha](https://code.google.com/p/mysql-master-ha/wiki/TableOfContents?tm=6)
* [mha4mysql-manager](https://github.com/yoshinorim/mha4mysql-manager)
* [MHA for MySQLとDeNAのオープンソースの話](http://www.slideshare.net/matsunobu/mha-for-mysqldena)
* [Automated master failover](http://www.slideshare.net/matsunobu/automated-master-failover/44)
* [masterha_master_switchについて（前編）](http://k-1-ne-jp.blogspot.jp/2013/02/masterhamasterswitch.html)
* [masterha_master_switchについて（後編）](http://k-1-ne-jp.blogspot.jp/2013/02/masterhamasterswitch_13.html)
* [MySQL を MHA + HAProxy で冗長化してみよう](http://heartbeats.jp/hbblog/2013/05/mysql-mha-haproxy.html)
