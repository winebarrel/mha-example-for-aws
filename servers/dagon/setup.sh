#!/bin/bash
if [ -z "$AWS_ACCESS_KEY_ID" -o -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo 'please set environment variables: AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY'
  exit 1
fi

export HOSTNAME=dagon
export BASE_DIR=`dirname $0`
. $BASE_DIR/../functions

copy_tools
write_hosts
set_hostname

sudo yum install -y nginx
update_ruby
sudo yum install -y make gcc mysql mysql-devel sqlite-devel
sudo yum --enablerepo=epel install -y nodejs
sudo yum --enablerepo=epel install -y siege

cat $BASE_DIR/dot.gemrc | write_to /root/.gemrc
cat $BASE_DIR/dot.gemrc > ~/.gemrc
sudo gem install rails -v 3.2.15
#sudo gem install mysql2

rails new ~/hello --skip-bundle
cat $BASE_DIR/nginx.conf | write_to /etc/nginx/nginx.conf
cat $BASE_DIR/unicorn.rb >> ~/hello/config/unicorn.rb

cd ~/hello
echo "gem 'mysql2'" >>  Gemfile
echo "gem 'unicorn'" >>  Gemfile
bundle install

VIRTUAL_IP_ADDRESS=`describe-route-tables | fgrep cthulhu | awk -F'[[/]' '{print $2}'`

cat <<EOF > config/database.yml
---
defaults: &defaults
  adapter: mysql2
  host: $VIRTUAL_IP_ADDRESS
  database: hello
  username: scott
  password: tiger
  encoding: utf8

development:
  <<: *defaults

test:
  <<: *defaults

production:
  <<: *defaults
EOF

rails g scaffold item name:string description:text
rake db:create
rake db:migrate

sudo service nginx start
bundle exec rake assets:precompile RAILS_ENV=production

echo $'\n'Please execute following commands:$'\n  '"cd ~/hello; unicorn_rails -c config/unicorn.rb -E production -D"$'\n  'curl -s localhost/items
