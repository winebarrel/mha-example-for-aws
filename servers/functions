function write_to {
  sudo sh -c "cat > '$1'" < /dev/stdin
}

function append_to {
  sudo sh -c "cat >> '$1'" < /dev/stdin
}

function mysql_query {
  mysql -u root -e "$1"
}

function write_hosts {
  write_to /etc/hosts <<EOF
127.0.0.1 localhost localhost.localdomain $HOSTNAME
EOF
  describe-instances | ruby -ryaml -e 'puts YAML.load(ARGF.read).select {|i| i[1] == "running" }.map {|i| i[2] + " " + i[6]["Name"] }.join("\n")' | fgrep -v $HOSTNAME | append_to /etc/hosts
}

function write_ssh_private_key {
  write_to /root/.ssh/id_rsa <<EOF
$MHA_SSH_PRIVATE_KEY
EOF
  sudo chmod 600 /root/.ssh/id_rsa
}

function append_ssh_public_key {
  append_to /root/.ssh/authorized_keys <<EOF
$MHA_SSH_PUBLIC_KEY
EOF
}

function set_hostname {
  sudo hostname $HOSTNAME
  sudo sed -i "s/localhost.localdomain/$HOSTNAME/" /etc/sysconfig/network
}

function allow_root_login {
  sudo sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/' /etc/ssh/sshd_config
  sudo service sshd reload
}

function create_mysql_user {
  mysql_query "GRANT REPLICATION SLAVE ON *.* TO repl@'%' IDENTIFIED BY 'repl'"
  mysql_query "GRANT ALL ON *.* TO maatkit@'localhost' IDENTIFIED BY 'maatkit'"
  mysql_query "GRANT ALL ON *.* TO maatkit@'%' IDENTIFIED BY 'maatkit'"
  mysql_query "GRANT ALL ON *.* TO scott@'%' IDENTIFIED BY 'tiger'"

  mysql_query "GRANT SUPER, PROCESS, RELOAD ON *.* TO mha@'%' IDENTIFIED BY 'mha'"
  mysql_query "GRANT ALL ON mysql.* TO mha@'%'"

  mysql_query "FLUSH PRIVILEGES"
}

function install_mha_node {
  wget https://mysql-master-ha.googlecode.com/files/mha4mysql-node-0.54-0.el6.noarch.rpm
  sudo yum --enablerepo=epel install -y mha4mysql-node-0.54-0.el6.noarch.rpm
}

function setup_slave {
  mysql_query "STOP SLAVE"
  mysql_query "CHANGE MASTER TO MASTER_HOST = 'cthulhu', MASTER_USER = 'repl', MASTER_PASSWORD = 'repl', MASTER_LOG_FILE = 'mysqld-bin.000002', MASTER_LOG_POS = 107"
  mysql_query "START SLAVE"
}

function install_mha_manager {
  wget https://mysql-master-ha.googlecode.com/files/mha4mysql-manager-0.55-0.el6.noarch.rpm
  sudo yum --enablerepo=epel install -y mha4mysql-manager-0.55-0.el6.noarch.rpm
}

function copy_tools {
  sudo cp -f $BASE_DIR/../../bin/* /usr/local/bin/
}

function disable_source_dest_check {
  modify-source-dest-check -d
}

function setup_sysctl {
  append_to /etc/sysctl.conf <<EOF
net.ipv4.conf.lo.arp_ignore = 1
net.ipv4.conf.lo.arp_announce = 2
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.all.arp_announce = 2
EOF
  sudo sysctl -p > /dev/null 2> /dev/null
}

function fix_sudoers_for_mysql {
  sudo cat /etc/sudoers.d/cloud-init | sed s/ec2-user/mysql/ | write_to /etc/sudoers.d/mysql
  sudo chmod 440 /etc/sudoers.d/mysql
}

function add_vip_to_lo {
  sudo ssh -t -o StrictHostKeyChecking=no mysql@$1 "sudo ip addr add $2/32 dev lo; sudo rm -f /etc/sudoers.d/mysql"
}

function create_ssh_key {
  sudo mkdir -p /var/lib/mysql/.ssh
  sudo chmod 700 /var/lib/mysql/.ssh
  sudo ssh-keygen -f /root/.ssh/id_rsa -P ''
  sudo sh -c 'cat /root/.ssh/id_rsa.pub >> /var/lib/mysql/.ssh/authorized_keys'
  sudo mv /root/.ssh/id_rsa /root/.ssh/id_rsa.pub /var/lib/mysql/.ssh/
  sudo chmod 600 /var/lib/mysql/.ssh/id_rsa /var/lib/mysql/.ssh/id_rsa.pub /root/.ssh/authorized_keys
  sudo chown mysql:mysql -R /var/lib/mysql/.ssh
}

function create_mha_log_dir {
  sudo mkdir /var/log/masterha
  sudo chown mysql:mysql /var/log/masterha
}

function copy_ssh_key_to_root_user {
  curl -s cthulhu:30080/id_rsa | write_to /root/.ssh/id_rsa
  curl -s cthulhu:30080/id_rsa.pub | write_to /root/.ssh/id_rsa.pub
  #sudo sh -c 'cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys'
  sudo chmod 600 /root/.ssh/id_rsa /root/.ssh/id_rsa.pub
  curl -s cthulhu:30080/stop
}

function copy_ssh_key_to_mysql_user {
  sudo mkdir -p /var/lib/mysql/.ssh
  sudo chmod 700 /var/lib/mysql/.ssh
  curl -s cthulhu:30080/id_rsa | write_to /var/lib/mysql/.ssh/id_rsa
  curl -s cthulhu:30080/id_rsa.pub | write_to /var/lib/mysql/.ssh/id_rsa.pub
  sudo sh -c 'cat /var/lib/mysql/.ssh/id_rsa.pub >> /var/lib/mysql/.ssh/authorized_keys'
  sudo chmod 600 /var/lib/mysql/.ssh/id_rsa /var/lib/mysql/.ssh/id_rsa.pub /var/lib/mysql/.ssh/authorized_keys
  sudo chown mysql:mysql -R /var/lib/mysql/.ssh
}

function start_ssh_key_server {
  nohup sudo ruby $BASE_DIR/ssh-key-server.rb > /dev/null 2> /dev/null &
}

function update_ruby {
  sudo yum remove -y ruby
  sudo yum install -y ruby19 ruby19-devel
}
