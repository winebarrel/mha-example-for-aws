#!/usr/bin/env ruby
require 'webrick'

$srv = WEBrick::HTTPServer.new(:Port => 30080)
$n = 0

$srv.mount_proc('/') do |req, res|
  $n += 1

  case req.path
  when '/id_rsa'
    res.body = File.read('/var/lib/mysql/.ssh/id_rsa')
  when '/id_rsa.pub'
    res.body = File.read('/var/lib/mysql/.ssh/id_rsa.pub')
  when '/stop'
    $srv.shutdown
  end
end

trap(:INT) { $srv.shutdown }
$srv.start
