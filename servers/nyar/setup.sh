#!/bin/bash
if [ -z "$AWS_ACCESS_KEY_ID" -o -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo 'please set environment variables: AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY'
  exit 1
fi

export HOSTNAME=nyar
export BASE_DIR=`dirname $0`
. $BASE_DIR/../functions

sudo yum install -y mysql-server

copy_tools
write_hosts
copy_ssh_key_to_mysql_user
set_hostname
#allow_root_login
disable_source_dest_check
setup_sysctl
create_mha_log_dir
fix_sudoers_for_mysql

cat $BASE_DIR/my.cnf | write_to /etc/my.cnf
sudo chkconfig mysqld on
sudo service mysqld start
create_mysql_user
setup_slave

sudo yum --enablerepo=epel install -y maatkit

install_mha_node
