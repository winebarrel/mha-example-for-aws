#!/bin/bash
if [ -z "$AWS_ACCESS_KEY_ID" -o -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo 'please set environment variables: AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY'
  exit 1
fi

export HOSTNAME=yog
export BASE_DIR=`dirname $0`
. $BASE_DIR/../functions

copy_tools
write_hosts
copy_ssh_key_to_root_user
set_hostname

sudo yum install -y mysql
update_ruby
sudo yum install -y make gcc mysql-devel
sudo gem install mysql2

install_mha_node
install_mha_manager
cat $BASE_DIR/app1.cnf | write_to /etc/app1.cnf
cat $BASE_DIR/master_ip_failover | write_to /usr/local/bin/master_ip_failover
cat $BASE_DIR/master_ip_online_change | write_to /usr/local/bin/master_ip_online_change
cat $BASE_DIR/restore-repl | write_to /usr/local/bin/restore-repl
sudo chmod 755 /usr/local/bin/master_ip_failover /usr/local/bin/master_ip_online_change /usr/local/bin/restore-repl

cat $BASE_DIR/mha.conf | write_to /etc/init/mha.conf
sudo initctl reload-configuration

sudo yum --enablerepo=epel install -y maatkit

echo $'\n'Please execute following commands:$'\n  '"sudo initctl list | grep mha"$'\n  'sudo masterha_check_ssh --conf=/etc/app1.cnf$'\n  'sudo masterha_check_repl --conf /etc/app1.cnf$'\n  'mk-slave-find -h cthulhu -u maatkit -p maatkit --report-format hostname
