#!/bin/bash
set -e
MASTER_HOST=cthulhu

if [ -z "$AWS_ACCESS_KEY_ID" -o -z "$AWS_SECRET_ACCESS_KEY" ]; then
  echo 'please set environment variables: AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY'
  exit 1
fi

. `dirname $0`/../functions

sudo cp -f `dirname $0`/../../bin/* /usr/local/bin/
TABLE_SIZE=`describe-route-tables | ruby -ryaml -e 'puts YAML.load(ARGF.read).keys.length'`

if [ $TABLE_SIZE -eq 1 ]; then
  ROUTE_TABLE_ID=`describe-route-tables | ruby -ryaml -e 'puts YAML.load(ARGF.read).keys.first'`
else
  PS3='select route table id: '

  select ROUTE_TABLE_ID in `describe-route-tables | ruby -ryaml -e 'puts YAML.load(ARGF.read).keys.join'`; do
    echo "ROUTE_TABLE_ID=$ROUTE_TABLE_ID"
  done
fi

echo -n 'input virtual ip address: '
read VIRTUAL_IP_ADDRESS
echo -n

INSTANCE_ID=`describe-instances -t "Name=$MASTER_HOST" | ruby -ryaml -e 'puts YAML.load(ARGF.read)[0][0]'`
create-route -t $ROUTE_TABLE_ID -d ${VIRTUAL_IP_ADDRESS}/32 -i $INSTANCE_ID

write_to /etc/sysconfig/mha.yml <<EOF
---
aws_access_key_id: $AWS_ACCESS_KEY_ID
aws_secret_access_key: $AWS_SECRET_ACCESS_KEY
route_table_id: $ROUTE_TABLE_ID
virtual_ip_address: $VIRTUAL_IP_ADDRESS
app_user: scott@'%'
EOF
sudo chmod 600 /etc/sysconfig/mha.yml

add_vip_to_lo cthulhu $VIRTUAL_IP_ADDRESS
add_vip_to_lo hastur $VIRTUAL_IP_ADDRESS
add_vip_to_lo nyar $VIRTUAL_IP_ADDRESS

echo Please execute following commands:$'\n  'describe-route-tables$'\n  'mysql -h $VIRTUAL_IP_ADDRESS -uscott -ptiger -e '"'"SHOW VARIABLES LIKE 'hostname'"'"'
